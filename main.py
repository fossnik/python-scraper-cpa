import requests
from bs4 import BeautifulSoup
import csv

def get_links_from_page(index_url):
    page = requests.get(index_url)
    soup = BeautifulSoup(page.content, "html.parser")
    table = soup.find("div", class_="listing-display")
    link_elements = table.findAll("a", class_="btn-primary")

    urls = []
    for link_element in link_elements:
        profile_url = link_element["href"]
        urls.append(profile_url)
        print(profile_url, end="\n")

    return urls


def get_links_from_all_pages():
    all_links = []

    page1_links = get_links_from_page("https://cpadirectory.com/certified-public-accountants/search?searchby=state&state=RI")
    all_links += page1_links

    for page_number in range(2, 96, 1):
        query_url = "https://cpadirectory.com/certified-public-accountants/search?searchby=state&state=RI" + "&page=" + str(page_number)
        profile_urls = get_links_from_page(query_url)
        all_links += profile_urls

    return all_links


links = get_links_from_all_pages()

# open a file for writing
data_file = open('profiles.csv', 'w')
# create the csv writer object
csv_writer = csv.writer(data_file)
for profile_url in links:
    csv_writer.writerow(profile_url)
data_file.close()