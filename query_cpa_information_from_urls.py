from bs4 import BeautifulSoup
from lxml import etree
import requests
import pandas as pd
import csv

import_object = pd.read_csv('profiles.csv')

profile_links = []
for url in import_object.values:
    profile_links.append(url[0])

HEADERS = ({'User-Agent':
                'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 \
                (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36', \
            'Accept-Language': 'en-US, en;q=0.5'})

# open a file for writing
data_file = open('data_file.csv', 'w')

# create the csv writer object
csv_writer = csv.writer(data_file)

count = 0
for profile_url in profile_links:
    count += 1
    webpage = requests.get(profile_url, headers=HEADERS)
    soup = BeautifulSoup(webpage.content, "html.parser")

    detail_sections = soup.select("section.detail-section")
    paragraph_zero = detail_sections[0]
    paragraph_three = detail_sections[2]

    p3_object = paragraph_three.text.split('\n')
    # dom = etree.HTML(str(soup))

    cpa_object = {
        "profile_url": profile_url,
        "license_number": paragraph_zero.text.split('\n')[4].split(' ')[-1],
        "name": p3_object[6].strip(),
        "street": p3_object[7].strip(),
        "city": p3_object[8].strip(),
        "phone": p3_object[15].strip(),
        "fax": p3_object[19].strip(),
        "website": p3_object[23].strip()
    }

    print([count, cpa_object])
    csv_writer.writerow(cpa_object.values())

data_file.close()
